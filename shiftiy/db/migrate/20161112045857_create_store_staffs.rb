class CreateStoreStaffs < ActiveRecord::Migration
  def change
    create_table :store_staffs do |t|
      t.integer :store_id
      t.integer :staff_id

      t.timestamps null: false
    end
  end
end
