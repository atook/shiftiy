class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :name
      t.string :email
      t.string :image_url
      t.time :open_time
      t.time :close_time
      t.string :tel
      t.string :address

      t.timestamps null: false
    end
  end
end
