class CreateShifts < ActiveRecord::Migration
  def change
    create_table :shifts do |t|
      t.integer :store_staff_id
      t.date :date
      t.time :start_time
      t.time :end_time
      t.time :rest_time

      t.timestamps null: false
    end
  end
end
