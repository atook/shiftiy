$(function() {
  $('#calendar').fullCalendar({
    lang: 'ja',
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },

    defaultView: 'month',
    slotMinutes: 20,
    businessHours: true,
    editable: true,
    defaultDate: '2016-01-12',
    slotEventOverlap:false,

    snapMinutes: 15,
    buttonText: {
      prev:     '<', // <
      next:     '>', // >
      prevYear: '<<',  // <<
      nextYear: '>>',  // >>
      today:    '今日',
      month:    '月',
      week:     '週',
      day:      '日'
    },
    monthNames: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
    monthNamesShort: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
    dayNames: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
    dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],

    events: {
      url: "/shifts/events",
      type: "POST",
      data: {
        shift_id: 1
      },
      error: function() {
        console.log("error");
      }
    },

    eventColor: '#378006',



    // カレンダーのどこかをクリックしたらイベントを作る
    dayClick: function(date, allDay, jsEvent, view) {
      var title = prompt('予定を入力してください:',jsEvent.title);
      if(title && title != ""){
        $('#calendar').fullCalendar('addEventSource', [{
          title: title,
          start: date,
          allDay: allDay
        }]);
      }

    },

    // イベントをクリックしたらタイトルをhogeに変える
    eventClick: function(calEvent, element) {
      var title = prompt('予定を入力してください:',calEvent.title);
      if(title && title != ""){
        console.log(title);
        calEvent.title = title;
        calEvent.start = '2016-01-22T06:00:00';
        calEvent.allDay = false
        //calEvent.end = '2014-01-22T09:00:00';
        $('#calendar').fullCalendar('updateEvent',calEvent);
      }
      else{
        $('#calendar').fullCalendar('removeEvent',calEvent.id);
      }
    },

  });
  $("#save").click(function () {
    var eventsFromCalendar = eventsFormat($('#calendar').fullCalendar('clientEvents'));
    console.log(JSON.stringify(eventsFromCalendar));
    $.ajax(
      {
        url: '/shifts/submit',
        //url: 'shifts/update',
        type: 'POST',
        traditional: true,
        data: { eventsJson: JSON.stringify(eventsFromCalendar) },
        dataType: "json",
        success: function (response) {
          console.log(response);
        },
        error: function (xhr) {
          console.error(xhr);
        }

      });
    });
  });

  function eventsFormat(orgEvents) {
    var events = [];

    console.log(orgEvents)

    for (var key in orgEvents) {
      var orgEvent = orgEvents[key];
      events.push({
        end: orgEvent.end ? orgEvent.end._i : "",
        start: orgEvent.start ? orgEvent.start._i : "",
        id: orgEvent._id,
        title: orgEvent.title
      });
    }

    return events
  }
