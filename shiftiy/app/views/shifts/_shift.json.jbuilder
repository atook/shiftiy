json.extract! shift, :id, :store_staff_id, :date, :start_time, :end_time, :rest_time, :created_at, :updated_at
json.url shift_url(shift, format: :json)