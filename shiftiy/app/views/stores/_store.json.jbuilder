json.extract! store, :id, :name, :email, :image_url, :open_time, :close_time, :tel, :address, :created_at, :updated_at
json.url store_url(store, format: :json)