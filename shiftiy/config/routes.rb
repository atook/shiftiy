Rails.application.routes.draw do

  root 'home#top'
  resources :shifts
  resources :stores
  resources :staffs

  post "shifts/events" => "shifts#get_events"
  post "shifts/submit" => "shifts#submit"


end
