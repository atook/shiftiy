# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Store.create(name: "MICHELKO",email:"michelko@hoge.com",tel:"080-1234-5678",address:"福岡県飯塚市12345")

Position.create(name:"アルバイト", wage:900)
Position.create(name:"アルバイトリーダー", wage:1200)


Pattern.create(name:"朝勤", start_time:"9:00", end_time:"15:00", require_count:3)
Pattern.create(name:"昼勤", start_time:"14:00", end_time:"19:00", require_count:3)
Pattern.create(name:"夕勤", start_time:"18:00", end_time:"23:00", require_count:3)


Staff.create(name:"中野慶事", email:"keiji@hoge.com", position_id:1, priority:3)
Staff.create(name:"坂本冬子", email:"ryoma@hoge.com", position_id:2, priority:5)
Staff.create(name:"中室慎吾", email:"shingo@hoge.com", position_id:1, priority:2)
Staff.create(name:"早見恵美", email:"emi@hoge.com", position_id:1, priority:2)
Staff.create(name:"西裕太", email:"yuya@hoge.com", position_id:1, priority:3)
Staff.create(name:"前川功", email:"ko@hoge.com", position_id:2, priority:3)
Staff.create(name:"佐藤伸治", email:"shinji@hoge.com", position_id:1, priority:5)
Staff.create(name:"鈴木寛", email:"kan@hoge.com", position_id:1, priority:2)
Staff.create(name:"長谷川学", email:"manabu@hgoe.com", position_id:1, priority:3)
Staff.create(name:"木戸隼太", email:"shunta@hoge.com", position_id:1, priority:3)
Staff.create(name:"早川啓太", email:"keiji@hoge.com", position_id:1, priority:3)
Staff.create(name:"須田洋介", email:"yosuke@hoge.com", position_id:2, priority:5)
Staff.create(name:"小南遼太", email:"ryota@hoge.com", position_id:1, priority:4)
Staff.create(name:"日野駿太", email:"shuto@hgoe.com", position_id:1, priority:4)
Staff.create(name:"大池忍", email:"shinobu@hoge.com", position_id:1, priority:2)

for i in 1..30 do
  (Date.today.beginning_of_week..Date.today.end_of_week).each do |date|
    weekDate = date.strftime("%Y-%m-%d")
    for k in 1..3 do
      rand = Random.rand(10.0).to_i
      if rand <= 4
        Shift.create(state:1,store_id:1,staff_id: i,date:"#{weekDate}",pattern_id: k)
      end
    end
  end
end
