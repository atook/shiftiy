class ChangeColumnToShift < ActiveRecord::Migration
  def change
    add_column :shifts,:staff_id,:integer
    add_column :shifts,:store_id,:integer
    remove_column :shifts,:store_staff_id,:integer
  end
end
