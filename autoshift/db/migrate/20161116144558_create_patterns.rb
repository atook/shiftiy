class CreatePatterns < ActiveRecord::Migration
  def change
    create_table :patterns do |t|
      t.string :name
      t.integer :require_count
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps null: false
    end
  end
end
