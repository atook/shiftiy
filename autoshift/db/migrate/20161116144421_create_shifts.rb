class CreateShifts < ActiveRecord::Migration
  def change
    create_table :shifts do |t|
      t.integer :state
      t.integer :store_staff_id
      t.integer :pattern_id

      t.timestamps null: false
    end
  end
end
