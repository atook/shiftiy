class AddCountToPatterns < ActiveRecord::Migration
  def change
    add_column :patterns, :count, :integer
  end
end
