class ChangeColumnToPattern < ActiveRecord::Migration
  def change
    change_column :patterns, :start_time, :time
    change_column :patterns, :end_time, :time
  end
end
