json.extract! position, :id, :name, :wage, :created_at, :updated_at
json.url position_url(position, format: :json)