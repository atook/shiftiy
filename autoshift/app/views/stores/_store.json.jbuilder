json.extract! store, :id, :name, :email, :tel, :address, :created_at, :updated_at
json.url store_url(store, format: :json)