json.extract! staff, :id, :name, :email, :position_id, :priority, :created_at, :updated_at
json.url staff_url(staff, format: :json)