json.extract! pattern, :id, :name, :require_count, :start_time, :end_time, :created_at, :updated_at
json.url pattern_url(pattern, format: :json)