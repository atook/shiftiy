json.extract! shift, :id, :state, :store_id,:staff_id, :pattern_id, :created_at, :updated_at
json.url shift_url(shift, format: :json)
