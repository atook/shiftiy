class ShiftsController < ApplicationController
  before_action :set_shift, only: [:show, :edit, :update, :destroy]
  before_action :get_shift, only: [:index, :auto]
  before_action :get_pattern, only: [:index, :new]

  # GET /shifts
  # GET /shifts.json
  def index
    # @staffs = Staff.all
    # @shifts = Shift.all.order("pattern_id")
    today = Date.today
    @shifts = Shift.joins(:staff).select("staffs.*,shifts.*").order("pattern_id")

    @shifts_sort_pattern_id = @shifts.sort{|a,b|(a[:date] == b[:date])?
       a[:pattern_id] <=> b[:pattern_id] : a[:date] <=> b[:date]
       }

  end

  def auto
  end




  # GET /shifts/1
  # GET /shifts/1.json
  def show
  end

  # GET /shifts/new
  def new
    @shift = Shift.new
  end

  # GET /shifts/1/edit
  def edit
  end

  # POST /shifts
  # POST /shifts.json
  def create
    @shift = Shift.new(shift_params)

    respond_to do |format|
      if @shift.save
        format.html { redirect_to @shift, notice: 'Shift was successfully created.' }
        format.json { render :show, status: :created, location: @shift }
      else
        format.html { render :new }
        format.json { render json: @shift.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shifts/1
  # PATCH/PUT /shifts/1.json
  def update
    respond_to do |format|
      if @shift.update(shift_params)
        format.html { redirect_to @shift, notice: 'Shift was successfully updated.' }
        format.json { render :show, status: :ok, location: @shift }
      else
        format.html { render :edit }
        format.json { render json: @shift.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shifts/1
  # DELETE /shifts/1.json
  def destroy
    @shift.destroy
    respond_to do |format|
      format.html { redirect_to shifts_url, notice: 'Shift was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shift
      @shift = Shift.find(params[:id])
    end

    def get_shift
      @shifts = Shift.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shift_params
      params.require(:shift).permit(:state, :store_id,:staff_id, :pattern_id,:date)
    end

    def get_pattern
      @patterns = Pattern.all
    end

end
